'use Strict';

import gulp from 'gulp';
import gutil from 'gulp-util';
import gulpif from 'gulp-if';
import sass from 'gulp-sass';
import cleanCSS from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import bs from 'browser-sync';
import spritesmith from 'gulp.spritesmith';
import imagemin from 'gulp-imagemin';
import buffer from 'vinyl-buffer';
import csso from 'gulp-csso';
import merge from 'merge-stream';
import ignore from 'gulp-ignore';
import autoprefixer from 'gulp-autoprefixer';
import shell from 'gulp-shell';

const imgFiles = 'assets/images/**/*';
const spriteFiles = 'assets/images/sprite/**/*';
const imgDest = 'web/images';
const scssFiles = 'assets/scss/**/*.scss';
const scssDest = 'web/css';
const jsFiles = 'assets/js/**/*.js';
const jsDest = 'web/js';
const htmlFiles = [
  'app/Resources/views/**/*.html.twig',
  'src/AppBundle/Resources/views/Web/**/*.html.twig',
  'src/AppBundle/Resources/views/Admin/**/*.html.twig',
  'src/UserBundle/Resources/views/**/*.html.twig',
];
const jsMinFileName = 'bundle.min.js';
const jsFileName = 'bundle.js';
const autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};
const prod = gutil.env.type === 'prod';

gulp.task('build-css', () => {
  return gulp.src(scssFiles)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(scssDest))
    .pipe(bs.reload({ stream: true }));
});

gulp.task('build-js', () => {
  return gulp.src([jsFiles])
    .pipe(sourcemaps.init())
    .pipe(concat(jsFileName))
    .pipe(gulpif(prod, rename(jsMinFileName), gutil.noop()))
    .pipe(gulpif(prod, uglify(), gutil.noop()))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(jsDest));
});

gulp.task('liip', shell.task(["./liip.sh"]));

gulp.task('images', () => {
  gulp.src(imgFiles)
    .pipe(ignore.exclude(spriteFiles))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDest));
});

gulp.task('sprite', () => {
  const spriteData = gulp.src(spriteFiles)
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.css',
      imgPath: '/images/sprite.png',
    }));

  const imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(imgDest));

  const cssStream = spriteData.css
    .pipe(csso())
    .pipe(gulp.dest(scssDest));

  return merge(imgStream, cssStream);
});

gulp.task('watch-js', ['build-js'], (done) => {
  bs.reload();
  done();
});

gulp.task('default', ['build-css', 'build-js'], () => {
  gulp.start('build-css');
  gulp.start('build-js');
  gulp.start('images');
  gulp.start('sprite');

  bs.init({
    proxy: '127.0.0.1:8000',
    // browser: '/home/kalwyn/.local/share/umake/web/firefox-dev/firefox',
    browser: 'chromium-browser',
    rewriteRules: [
      {
        match: /<body/,
        fn: function (match) {
          return '<body data-no-turbolink="true" ';
        }
      }
    ]
  });

  gulp.watch(scssFiles, ['build-css']);
  gulp.watch(jsFiles, ['watch-js']);
  gulp.watch(imgFiles, ['sprite', 'images']);
  gulp.watch(htmlFiles).on('change', bs.reload);
});
