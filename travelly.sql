-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 24 Mai 2017 à 09:34
-- Version du serveur :  5.7.18-0ubuntu0.17.04.1
-- Version de PHP :  7.0.18-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `travelly`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `name`, `content`, `created_at`, `updated_at`, `user_id`, `icon`) VALUES
(1, 'Vol', '', '2017-05-16 00:00:00', NULL, 29, 'vol'),
(2, 'Hotêl', '', '2017-05-16 00:00:00', NULL, 16, 'hotel'),
(3, 'Séjour', '', '2017-05-16 00:00:00', NULL, 16, 'sejour'),
(4, 'Location', '', '2017-05-16 11:14:29', NULL, 16, 'location'),
(5, 'Autre', '', '2017-05-16 11:14:40', NULL, 16, 'autre');

-- --------------------------------------------------------

--
-- Structure de la table `deal`
--

CREATE TABLE `deal` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `price` double NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `deal`
--

INSERT INTO `deal` (`id`, `name`, `thumbnail`, `content`, `created_at`, `updated_at`, `category_id`, `user_id`, `price`, `slug`) VALUES
(1, 'Amazing australia', 'img-1.jpg', 'Become inspired to travel to Australia. Discover fantastic things to do & places to go.', '2017-05-16 00:00:00', NULL, 1, 16, 1095, 'amazing-australia'),
(2, 'Ancient south america', 'img-2.jpg', 'Become inspired to travel to South America. Discover fantastic things to do & places to go.', '2017-05-16 00:00:00', NULL, 1, 16, 1295, 'ancient-south-america'),
(3, 'Modern north america', 'img-3.jpg', 'Become inspired to travel to North America. Discover fantastic things to do & places to go.', '2017-05-16 00:00:00', NULL, 3, 19, 695, 'modern-north-america'),
(4, 'Classic europe', 'img-4.jpg', 'Become inspired to travel to Europe. Discover fantastic things to do & places to go.', '2017-05-16 00:00:00', NULL, 3, 16, 295, 'classic-europe');

-- --------------------------------------------------------

--
-- Structure de la table `deal_comment`
--

CREATE TABLE `deal_comment` (
  `id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `deal_comment`
--

INSERT INTO `deal_comment` (`id`, `content`, `created_at`, `updated_at`, `deal_id`, `user_id`) VALUES
(1, 'Premier commentaire !', '2017-05-18 00:00:00', NULL, 1, 16),
(2, 'Deuxième commentaire !', '2017-05-18 09:00:00', NULL, 1, 16),
(6, 'Troisième commentaire !', '2017-05-18 11:02:24', NULL, 1, 16),
(7, '4', '2017-05-18 11:31:36', NULL, 1, 16),
(8, '5', '2017-05-18 11:32:16', NULL, 1, 16),
(9, '6', '2017-05-18 11:33:57', NULL, 1, 16);

-- --------------------------------------------------------

--
-- Structure de la table `deal_tag`
--

CREATE TABLE `deal_tag` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id`, `user_id`, `first_name`, `last_name`, `email`, `content`, `created_at`, `status`) VALUES
(1, 16, 'root', 'admin', 'root@mail.com', 'azdqsgaqsdgfqsg', '2017-05-16 10:10:18', 0),
(9, 16, 'root', 'admin', 'root@mail.com', 'fdqdsfdqsfdqsf', '2017-05-19 09:22:02', 0),
(10, 16, 'root', 'admin', 'root@mail.com', 'Essai', '2017-05-19 09:24:58', 0),
(11, 16, 'root', 'admin', 'root@mail.com', 'Essai', '2017-05-19 09:25:39', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `firstName`, `lastName`) VALUES
(16, 'root', 'root', 'root@mail.com', 'root@mail.com', 1, NULL, '$2y$13$F0xOSVn3UJABfBchghQTZ.g1ibp36fgRdZ5z7bUD4rmnxU7q.pEAq', '2017-05-19 16:23:46', NULL, NULL, 'N;', 'root', 'admin'),
(17, 'aaaaa', 'aaaaa', 'aaaaa@a.com', 'aaaaa@a.com', 1, NULL, '$2y$13$2p/YI7vFM6h3zGeSMnSjkORjasXaC.O5e7m9K8CekUc5SJaicrhIC', NULL, NULL, NULL, 'N;', 'aaaaa', 'aaaaa'),
(19, 'adminuser', 'adminuser', 'admin@mail.com', 'admin@mail.com', 1, NULL, '$2y$13$fbhOzgOL/jChA/2K7xWeLOL3DK6sLkmVtjasuZNRPi0LnazREN6JK', '2017-05-23 09:12:05', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 'Admin', 'Root'),
(20, 'testuser', 'testuser', 'test@example.com', 'test@example.com', 1, NULL, '$2y$13$xUCs/eEbrZdTVNUktipR5u9kauPywn/cLh8QQfnVd7oHQAQ4N0n82', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(23, '1kqjfdhsfkljdhqsf', '1kqjfdhsfkljdhqsf', '1@example.com', '1@example.com', 1, NULL, '$2y$13$PbV1OgYP0ZAel2KkbuLnV.yu6qtb/tg9ox3GNQMv0C4dAIFlezuY2', NULL, NULL, NULL, 'a:0:{}', 'qsd', 'wxc'),
(24, '2', '2', '2@example.com', '2@example.com', 1, NULL, '$2y$13$cHs65o9UtJ1msyh3i637LuqPihrCz360iLSLmCPLfL.NCDNU3FacK', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(25, '3', '3', '3@example.com', '3@example.com', 1, NULL, '$2y$13$zBAddHIOr47B4m1WTWgeTOqQBScqDXcnPQKp.cA2bbWoVM90DO8Km', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(26, '4', '4', '4@example.com', '4@example.com', 1, NULL, '$2y$13$hDw6fGX88.XQcfumiF7GgeojGrB/BcWcpAdDpXCznFQ7.4YIq8Uja', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(27, '5', '5', '5@example.com', '5@example.com', 1, NULL, '$2y$13$GEwThImaLL84feAosHMzeu42nBj9sLLxT5dJtQLm9ydfrf0OopAVm', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(28, '6', '6', '6@example.com', '6@example.com', 1, NULL, '$2y$13$xAabG7QOa8EOoq5PkWlMcuw2F/lV7KuMZIqSmIxepflHkdVDIcWRq', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(29, '7', '7', '7@example.com', '7@example.com', 1, NULL, '$2y$13$jlUqKLK.5v12Lqk7nFz30OshgvxNYDWF716nTx/HG74PxikvBrDpq', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(30, '8', '8', '8@example.com', '8@example.com', 1, NULL, '$2y$13$q5V4O8O.YdncvtgEYh9BbOSzXRevpZ1IsdeQs.6UXrqi6wfyOShNq', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(31, '9', '9', '9@example.com', '9@example.com', 1, NULL, '$2y$13$W9QQHQV35pUMW3/umTJ.D.wTRoz8ZfR8ojAfTgW5LHDBE10bptZJu', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(32, '10', '10', '10@example.com', '10@example.com', 1, NULL, '$2y$13$qpZEFIX0cAJf7GGI89RyQ.SmmImBxYEKQ27MqXcBGB33G5Si9npTu', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(33, '11', '11', '11@example.com', '11@example.com', 1, NULL, '$2y$13$Ibw8Gz2TAoLizW7gvX/SPuXg0YYORo963eGSGAg6qlM66jkKwwCl6', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(34, '12', '12', '12@example.com', '12@example.com', 1, NULL, '$2y$13$pSC3sOSf.YCUuQZaW1muquE0wyO92cuTXRpiGtPjjNbtmmp1zpBlq', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(35, '13', '13', '13@example.com', '13@example.com', 1, NULL, '$2y$13$xIeI0kHzixC6CZKLbfEqU.wfbZkLqVRPDbJ8Ucf2QV9KvGrFJwDP2', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(36, '14', '14', '14@example.com', '14@example.com', 1, NULL, '$2y$13$tEpXa5R9ELz03JbtvwOSoOR8dXCZYEAtJ9BsDfax.z9Jh1hZfPHf2', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(37, '15', '15', '15@example.com', '15@example.com', 1, NULL, '$2y$13$4EsEuzRmLLR9HvEYoEYCKOA4sk7V2Rh7nTO0UPvwuiS16Kq/cw1RC', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(38, '16', '16', '16@example.com', '16@example.com', 1, NULL, '$2y$13$gKFwXvtcGN6gYzcQAaT0UOB9EP7na2AAS.bV4cQ5e38z8Rz.dzQSe', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(39, '17', '17', '17@example.com', '17@example.com', 1, NULL, '$2y$13$/qQdLfSUk9u2w7olaVDHTeHmBrXErPJyz3FWE5RGZk2ge4ThZeIWC', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(40, '18', '18', '18@example.com', '18@example.com', 1, NULL, '$2y$13$ajtvI6YtC9hGtpy4dhCPqu5vSKtasJPsJxf4yzUTpkrmZgVfx6/G6', NULL, NULL, NULL, 'a:0:{}', NULL, NULL),
(42, '20', '20', '20@example.com', '20@example.com', 1, NULL, '$2y$13$tyiwYSlTZO7VY72IIS/KB.5e3/0nqjXX4JZefZvEPgmAGRBNUxcbO', NULL, NULL, NULL, 'a:0:{}', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_64C19C1A76ED395` (`user_id`);

--
-- Index pour la table `deal`
--
ALTER TABLE `deal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E3FEC11612469DE2` (`category_id`),
  ADD KEY `IDX_E3FEC116A76ED395` (`user_id`);

--
-- Index pour la table `deal_comment`
--
ALTER TABLE `deal_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_706EDAE4F60E2305` (`deal_id`),
  ADD KEY `IDX_706EDAE4A76ED395` (`user_id`);

--
-- Index pour la table `deal_tag`
--
ALTER TABLE `deal_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_18C125BCBAD26311` (`tag_id`),
  ADD KEY `IDX_18C125BCA76ED395` (`user_id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FA76ED395` (`user_id`);

--
-- Index pour la table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_389B783A76ED395` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `deal`
--
ALTER TABLE `deal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `deal_comment`
--
ALTER TABLE `deal_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `deal_tag`
--
ALTER TABLE `deal_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `FK_64C19C1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `deal`
--
ALTER TABLE `deal`
  ADD CONSTRAINT `FK_E3FEC11612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_E3FEC116A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `deal_comment`
--
ALTER TABLE `deal_comment`
  ADD CONSTRAINT `FK_706EDAE4A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_706EDAE4F60E2305` FOREIGN KEY (`deal_id`) REFERENCES `deal` (`id`);

--
-- Contraintes pour la table `deal_tag`
--
ALTER TABLE `deal_tag`
  ADD CONSTRAINT `FK_18C125BCA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_18C125BCBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `tag`
--
ALTER TABLE `tag`
  ADD CONSTRAINT `FK_389B783A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
