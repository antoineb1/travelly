#!/usr/bin/env bash

sudo sysctl -w vm.max_map_count=262144
sudo docker-compose up -d
sleep 10
php bin/console fos:elastica:populate
