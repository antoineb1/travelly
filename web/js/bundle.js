$(document).ready(function() {
  $('.btn-banish').on('click', function(e) {

    var id = $(this).data("id");
    var $this = $(this);

    $.post(Routing.generate('admin_users_banish', {id: id}), function(data, status) {
      if (status === "success") {
        if (data['action'] === 'disable') {
          $this.removeClass('btn-warning') ;
          $this.addClass('btn-primary');
        } else if (data['action'] === 'enable') {
          $this.removeClass('btn-primary');
          $this.addClass('btn-warning') ;
        } else {
          console.error("Couldn't user");
        }
      }
    });
  });
});

$(document).ready(function() {
  $('.btn-delete').on('click', function(e) {
    console.log('value');
    var id = $(this).data("id");
    var type = $(this).data("type");
    var $this = $(this);

    switch(type) {
      case "deal":
        url = 'admin_deals_delete';
        break;
      case "user":
        url = 'admin_users_delete';
        break;
      case "category":
        url = 'admin_categories_delete';
        break;
      default:
        console.error("Unknown catgory in delete action");
        return;
    }

    $.post(Routing.generate(url, {id: id}), function(data, status) {
      if (status === "success") {
        $this.parent().parent().remove();
      }
    });
  });
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJhbmlzaC5qcyIsImRlbGV0ZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3JCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICQoJy5idG4tYmFuaXNoJykub24oJ2NsaWNrJywgZnVuY3Rpb24oZSkge1xuXG4gICAgdmFyIGlkID0gJCh0aGlzKS5kYXRhKFwiaWRcIik7XG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICAgICQucG9zdChSb3V0aW5nLmdlbmVyYXRlKCdhZG1pbl91c2Vyc19iYW5pc2gnLCB7aWQ6IGlkfSksIGZ1bmN0aW9uKGRhdGEsIHN0YXR1cykge1xuICAgICAgaWYgKHN0YXR1cyA9PT0gXCJzdWNjZXNzXCIpIHtcbiAgICAgICAgaWYgKGRhdGFbJ2FjdGlvbiddID09PSAnZGlzYWJsZScpIHtcbiAgICAgICAgICAkdGhpcy5yZW1vdmVDbGFzcygnYnRuLXdhcm5pbmcnKSA7XG4gICAgICAgICAgJHRoaXMuYWRkQ2xhc3MoJ2J0bi1wcmltYXJ5Jyk7XG4gICAgICAgIH0gZWxzZSBpZiAoZGF0YVsnYWN0aW9uJ10gPT09ICdlbmFibGUnKSB7XG4gICAgICAgICAgJHRoaXMucmVtb3ZlQ2xhc3MoJ2J0bi1wcmltYXJ5Jyk7XG4gICAgICAgICAgJHRoaXMuYWRkQ2xhc3MoJ2J0bi13YXJuaW5nJykgO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJDb3VsZG4ndCB1c2VyXCIpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgJCgnLmJ0bi1kZWxldGUnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgY29uc29sZS5sb2coJ3ZhbHVlJyk7XG4gICAgdmFyIGlkID0gJCh0aGlzKS5kYXRhKFwiaWRcIik7XG4gICAgdmFyIHR5cGUgPSAkKHRoaXMpLmRhdGEoXCJ0eXBlXCIpO1xuICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG5cbiAgICBzd2l0Y2godHlwZSkge1xuICAgICAgY2FzZSBcImRlYWxcIjpcbiAgICAgICAgdXJsID0gJ2FkbWluX2RlYWxzX2RlbGV0ZSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcInVzZXJcIjpcbiAgICAgICAgdXJsID0gJ2FkbWluX3VzZXJzX2RlbGV0ZSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcImNhdGVnb3J5XCI6XG4gICAgICAgIHVybCA9ICdhZG1pbl9jYXRlZ29yaWVzX2RlbGV0ZSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgY29uc29sZS5lcnJvcihcIlVua25vd24gY2F0Z29yeSBpbiBkZWxldGUgYWN0aW9uXCIpO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgJC5wb3N0KFJvdXRpbmcuZ2VuZXJhdGUodXJsLCB7aWQ6IGlkfSksIGZ1bmN0aW9uKGRhdGEsIHN0YXR1cykge1xuICAgICAgaWYgKHN0YXR1cyA9PT0gXCJzdWNjZXNzXCIpIHtcbiAgICAgICAgJHRoaXMucGFyZW50KCkucGFyZW50KCkucmVtb3ZlKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xufSk7XG4iXX0=
