$(document).ready(function() {
  $('.btn-banish').on('click', function(e) {

    var id = $(this).data("id");
    var $this = $(this);

    $.post(Routing.generate('admin_users_banish', {id: id}), function(data, status) {
      if (status === "success") {
        if (data['action'] === 'disable') {
          $this.removeClass('btn-warning') ;
          $this.addClass('btn-primary');
        } else if (data['action'] === 'enable') {
          $this.removeClass('btn-primary');
          $this.addClass('btn-warning') ;
        } else {
          console.error("Couldn't user");
        }
      }
    });
  });
});
