$(document).ready(function() {
  $('.btn-delete').on('click', function(e) {
    console.log('value');
    var id = $(this).data("id");
    var type = $(this).data("type");
    var $this = $(this);

    switch(type) {
      case "deal":
        url = 'admin_deals_delete';
        break;
      case "user":
        url = 'admin_users_delete';
        break;
      case "category":
        url = 'admin_categories_delete';
        break;
      default:
        console.error("Unknown catgory in delete action");
        return;
    }

    $.post(Routing.generate(url, {id: id}), function(data, status) {
      if (status === "success") {
        $this.parent().parent().remove();
      }
    });
  });
});
