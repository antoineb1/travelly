<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ElasticSearch
{

    protected $container;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    public function generateQuery($params)
    {

        $finder = $this->container->get('fos_elastica.finder.app.deal');
        $boolQuery = new \Elastica\Query\BoolQuery();

        $fieldQuery = new \Elastica\Query\Match();

        $fieldQuery->setFieldFuzziness('name', 1);
        $fieldQuery->setFieldQuery('name', $params);

        $boolQuery->addShould($fieldQuery);

        $results = $finder->find($boolQuery);

        return $results;

    }
}
