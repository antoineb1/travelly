<?php

namespace AppBundle\Controller\API;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use AppBundle\Entity\Message;


/**
 * @Route("/messages")
 */
class MessagesController extends FOSRestController
{
    /**
     * @Get("")
     */
    public function getMessagesAction(Request $request)
    {
        $limit = $request->query->get("limit", 10);
        $offset = $request->query->get("offset", 0);

        $em = $this->getDoctrine()->getManager();
        $messages = $em->getRepository("AppBundle:Message")->findBy([], [], $limit, $offset);

        return $messages;
    }

    /**
     * @Get("/{id}", requirements={"id": "\d+"})
     */
    public function getMessageAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository("AppBundle:Message")->findBy(["id" => $id]);

        return $message;
    }

    /**
     * @Post("/create")
     * @RequestParam(name="firstName", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="lastName", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="email", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="content", nullable=false, allowBlank=false, strict=true)
     */
    public function postMessageAction(Request $request, ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $message = new Message;

        $message->setFirstName($data["firstName"]);
        $message->setLastName($data["lastName"]);
        $message->setEmail($data["email"]);
        $message->setContent($data["content"]);

        $em->persist($message);
        $em->flush();

        return $message;
    }

    /**
     * @Put("/edit")
     * @RequestParam(name="id", requirements="\d+", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="firstName", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="lastName", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="email", nullable=false, allowBlank=false, strict=true)
     * @RequestParam(name="content", nullable=false, allowBlank=false, strict=true)
     */
    public function putMessageAction(Request $request, ParamFetcher $paramFetcher)
    {
        $paramFetcher->all();
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository("AppBundle:Message")->find($paramFetcher->get('id'));

        $message->setFirstName($data["firstName"]);
        $message->setLastName($data["lastName"]);
        $message->setEmail($data["email"]);
        $message->setContent($data["content"]);

        $em->persist($message);
        $em->flush();

        return $message;
    }

    /**
     * @Delete("/{id}", requirements={"id": "\d+"})
     */
    public function deleteMessage(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository("AppBundle:Message")->findOneBy(["id" => $id]);
        $em->remove($message);
        $em->flush();

        return $message;
    }
}
