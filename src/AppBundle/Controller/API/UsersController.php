<?php

namespace AppBundle\Controller\API;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/users")
 */
class UsersController extends FOSRestController {

    /**
     * @Post("/login")
     * @ApiDoc(
     *  resource=true,
     *  description="Form to log the user",
     *  requirements={
     *      {
     *          "name"="email",
     *          "dataType"="string",
     *          "description"="User email"
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "description"="User password"
     *      }
     *  }
     * )
     */
    public function loginAction(Request $request)
    {
        $manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');

        $email = $request->request->get('email');
        $password = $request->request->get('password');

        $em = $this->getDoctrine()->getManager();

        $bool = false;

        $response = [
            'status' => false
        ];

        $user = $em->getRepository("AppBundle:User")->findOneBy(["email" => $email]);
        if ($user == null) {
            throw new HttpException(404, "Can't found the user");
        }

        $encoder = $factory->getEncoder($user);
        $bool = ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) ? true : false;

        if($bool) {
            $response = [
                'status' => true,
                'session' => [
                    'id' => $user->getId(),
                    'firstName' => $user->getFirstName(),
                    'lastName' => $user->getLastName(),
                ],
            ];
        }

        return $response;
    }

    /**
     * @Post("/register")
     * @ApiDoc(
     *  description="Form to register the user",
     *  requirements={
     *      {
     *          "name"="email",
     *          "dataType"="string",
     *          "description"="User email"
     *      },
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "description"="Username"
     *      },
     *      {
     *          "name"="plainPassword[first]",
     *          "dataType"="string",
     *          "description"="User password"
     *      },
     *      {
     *          "name"="plainPassword[second]",
     *          "dataType"="string",
     *          "description"="User confirmation password"
     *      },
     *      {
     *          "name"="firstName",
     *          "dataType"="string",
     *          "description"="User first name"
     *      },
     *      {
     *          "name"="lastName",
     *          "dataType"="string",
     *          "description"="User last name"
     *      }
     *  }
     * )
     */
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(['csrf_protection' => false, 'allow_extra_fields' => true]);
        $form->remove('recaptcha');
        $form->setData($user);
        $form->submit($request->request->all());


        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $user;
        } else {
            return $form;
        }
    }

    /**
     * @Post("/update/{username}")
     */
    public function updateUserPasswordAction(Request $request, $username)
    {
        //$user = $this->getDoctrine()->getManager()->getRepository("AppBundle:User")->findOneById($id);
        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm(['csrf_protection' => false, 'allow_extra_fields' => true]);
        $form->setData($user);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

            $userManager->updateUser($user);

            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $user;
        } else {
            throw new HttpException(400, "invalid User form");
        }

    }
}


