<?php

namespace AppBundle\Controller\API;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


/**
 * @Route("/deals")
 */
class DealsController extends FOSRestController
{
    /**
     * @Get("")
     * @ApiDoc(
     *  resource=true,
     *  description="Get all deals",
     *  filters={
     *      {
     *          "name"="limit",
     *          "dataType"="integer",
     *          "description"="Limit"
     *      },
     *      {
     *          "name"="offset",
     *          "dataType"="integer",
     *          "description"="Offset"
     *      }
     *  }
     * )
     */
    public function getDealsAction(Request $request)
    {
		$limit = $request->query->get("limit", 10);
		$offset = $request->query->get("offset", 0);

        $em = $this->getDoctrine()->getManager();
		$deals = $em->getRepository("AppBundle:Deal")->findBy([], [], $limit, $offset);

        return $deals;
    }

    /**
     * @Get("/{id}", requirements={"id": "\d+"})
     * @ApiDoc(
     *  description="Get a deal by Id"
     * )
     */
    public function dealAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $deal = $em->getRepository("AppBundle:Deal")->findOneBy(["id" => $id]);

        return $deal;
    }

    /**
     * @Post("/search")
     * @ApiDoc(
     *  description="Get all deals by name",
     *  filters={
     *      {
     *          "name"="limit",
     *          "dataType"="integer",
     *          "description"="Limit"
     *      },
     *      {
     *          "name"="offset",
     *          "dataType"="integer",
     *          "description"="Offset"
     *      }
     *  },
     *  requirements={
     *      {
     *          "name"="name",
     *          "dataType"="string",
     *          "description"="Name of the deal"
     *      }
     *  }
     * )
     */
    public function getDealByNameAction(Request $request)
    {
        $name = $request->request->get("name", null);
        $limit = $request->query->get("limit", 10);
        $offset = $request->query->get("offset", 0);

        $em = $this->getDoctrine()->getManager();

        $deal = $em->createQuery("
                    SELECT p FROM AppBundle:Deal p WHERE p.name LIKE :key 
                ")->setParameters([
                    'key' => '%'.$name.'%',
                ]);
        
        return $deal
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();


    }
}
