<?php

namespace AppBundle\Controller\API;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use AppBundle\Entity\DealComment;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


/**
 * @Route("/deals")
 */
class DealsCommentsController extends FOSRestController
{

    /**
     * @Post("/{id}/comment")
     * @ApiDoc(
     *  description="Create a deal comment",
     *  requirements={
     *      {
     *          "name"="content",
     *          "dataType"="string",
     *          "description"="Comment"
     *      },
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "description"="User id"
     *      }
     *  }
     * )
     */
    public function postDealCommentAction(Request $request, $id)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $deal = $em->getRepository("AppBundle:Deal")->find($id);
        $user = $em->getRepository("AppBundle:USer")->find($data["userId"]);


        $comment = new DealComment;
        $comment->setDeal($deal);
        $comment->setContent($data["content"]);
        $comment->setUser($user);

        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    /**
     * @Put("/comment/{id}")
     * @ApiDoc(
     *  description="Edit a deal comment",
     *  requirements={
     *      {
     *          "name"="content",
     *          "dataType"="string",
     *          "description"="Comment"
     *      }
     *  }
     * )
     */
    public function putDealCommentAction(Request $request, $id)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository("AppBundle:DealComment")->find($id);

        $comment->setContent($data["content"]);

        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    /**
     * @Delete("/{id}", requirements={"id": "\d+"})
     */
    public function deleteDealComment(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository("AppBundle:DealComment")->findOneBy(["id" => $id]);
        $em->remove($comment);
        $em->flush();

        return $comment;
    }
}
