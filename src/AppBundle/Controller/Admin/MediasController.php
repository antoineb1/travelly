<?php
namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Media;

class MediasController extends Controller
{

    private function checkMimeType($uploadedFile)  {
        $mime = $uploadedFile->getMimeType();

        switch ($mime) {
            case 'image/jpeg':
                return true;
                break;

            case 'image/png':
                return true;
                break;

            case 'image/jpg':
                return true;
                break;
        }

        return false;
    }

    /**
     * Upload media
     * 
     * @Route("/medias",
     *     name = "app_medias_index"
     * )
     * @Template("AppBundle:Admin:Medias/dropzone.html.twig")
     */
    public function indexAction(Request $request)
    {

    }

    /**
     * Upload media
     * 
     * @Route("/medias/upload",
     *     name = "admin_medias_upload",
     * )
     * @param  Request
     * @return json
     */
    public function uploadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $media = new Media();
        $mediasFolder = $media->getUploadRootDir();
        $i = 0;


        foreach($request->files as $uploadedFile) {
            $response = $this->checkMimeType($uploadedFile);

            if($response == false) {
                return new JsonResponse([
                    "fileError" => true,
                ]);
            }

            $name = md5(uniqid());

            try {
                $extension = $uploadedFile->guessExtension();
            } catch (Exception $e) {
                $extension = "png";
            }

            $file_name = $name.".".$extension;

            $file = $uploadedFile->move($mediasFolder, $file_name);
            
            $media->setFilename($file_name);
            $em->persist($media);
            $em->flush();

            $i++;
        }


        if($i > 0) {
        	$url = $this->container->get('router')->getContext()->getBaseUrl(); 

	        return new JsonResponse(
        		[
                    "id" => $media->getId(),
					"file_src" => $url . '/medias/' . $name.".".$extension,
            		"file_name" => $name . "." . $extension,
        		]
	        );
        }

        exit;
    }
}

