<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\DealType;

class MessagesController extends Controller
{
    /**
     * @Route("/messages", name="admin_messages_manage")
     */
    public function manageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $messages = $em->getRepository('AppBundle:Message')->findAll();

        return $this->render('AppBundle:Admin/Deals:index.html.twig', [
            'messages' => $messages
        ]);
    }
}
