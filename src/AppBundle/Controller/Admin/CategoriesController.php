<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\CategoryType;

class CategoriesController extends Controller
{
    /**
     * @Route("/Categories", name="admin_categories_manage")
     */
    public function manageAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT c FROM AppBundle:Category c";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Admin/Categories:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/categories/view/{id}", name="admin_categories_view", requirements={"id": "\d+"})
     */
    public function viewAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->findOneBy(['id' => $id]);

        return $this->render('AppBundle:Admin/Categories:view.html.twig', [
            'category' => $category
        ]);
    }

    /**
     * @Route("/categories/edit/{id}", name="admin_categories_edit", requirements={"id": "\d+"})
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->findOneBy(['id' => $id]);

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_categories_manage');
        }

        return $this->render('AppBundle:Admin/Categories:edit.html.twig', [
            'category' => $category, 'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/categories/delete/{id}", name="admin_categories_delete", options={"expose"=true}, requirements={"id": "\d+"})
     */
    public function deleteAction(Request $request)
    {
        return JsonResponse::fromJsonString('{ "action": "remove"}');
    }
}
