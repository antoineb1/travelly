<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\DealType;

class DealsController extends Controller
{
    /**
     * @Route("/deals", name="admin_deals_manage")
     */
    public function manageAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT d FROM AppBundle:Deal d";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Admin/Deals:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/deals/edit/{id}", name="admin_deals_edit", requirements={"id": "\d+"})
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $deal = $em->getRepository('AppBundle:Deal')->findOneBy(['id' => $id]);

        $form = $this->createForm(DealType::class, $deal);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($deal);
            $em->flush();

            return $this->redirectToRoute('admin_deals_manage');
        }

        return $this->render('AppBundle:Admin/Deals:edit.html.twig', [
            'deal' => $deal, 'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/deals/delete/{id}", name="admin_deals_delete", options={"expose"=true}, requirements={"id": "\d+"})
     */
    public function deleteAction(Request $request)
    {
        return JsonResponse::fromJsonString('{ "action": "remove"}');
    }
}
