<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UsersController extends Controller
{
    /**
     * @Route("/users", name="admin_users_manage")
     */
    public function manageAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT u FROM AppBundle:User u";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AppBundle:Admin/Users:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/users/view/{id}", name="admin_users_view", requirements={"id": "\d+"})
     */
    public function viewAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);

        return $this->render('AppBundle:Admin/Users:view.html.twig', [
            'user' => $user
        ]);
    }


    /**
     * @Route("/users/edit/{id}", name="admin_users_edit", requirements={"id": "\d+"})
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);

        $formFactory = $this->get('fos_user.registration.form.factory');
        $form = $formFactory->createForm();
        $form->remove("recaptcha");
        $form->setData($user);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl("admin_users_manage"));
        }


        return $this->render('AppBundle:Admin/Users:edit.html.twig', [
            'user' => $user, 'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/users/banish/{id}", name="admin_users_banish", requirements={"id": "\d+"}, options={"expose"=true})
     */
    public function banishAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);


        if ($user == null) {
            return JsonResponse::fromJsonString('{"action": "null"}');
        }

        if ($user->isEnabled()) {
            $user->setEnabled(false);
            $em->persist($user);
            $em->flush();
            return JsonResponse::fromJsonString('{ "action": "disable"}');
        }

        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();
        return JsonResponse::fromJsonString('{ "action": "enable"}');
    }

    /**
     * @Route("/users/delete/{id}", name="admin_users_delete", requirements={"id": "\d+"}, options={"expose"=true})
     */
    public function deleteAction(Request $request, $id)
    {
        //$em = $this->getDoctrine()->getManager();
        //$user = $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);

        //if ($user == null) {
        //    return JsonResponse::fromJsonString('{"action": "null"}');
        //}

        //$em->remove($user);
        //$em->flush();
        return JsonResponse::fromJsonString('{ "action": "remove"}');
    }
}
