<?php

namespace AppBundle\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ContactType;
use AppBundle\Entity\Message;

class PagesController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $deals = $em->getRepository('AppBundle:Deal')->findBy([], [], 4);

        return $this->render('AppBundle:Web/Pages:home.html.twig', [
            'deals' => $deals
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $msg = new Message();
        $user = $this->getUser();

        if ($user != null) {
            $msg->setFirstName($user->getFirstName());
            $msg->setLastName($user->getLastName());
            $msg->setEmail($user->getEmail());
            $msg->setUser($user);
        }

        $form = $this->createForm(ContactType::class, $msg);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($msg);
            $em->flush();

            $sendinblue = $this->get('sendinblue_api');

            $data = array( "to" => array(
                "kynarth.alseif@gmail.com"=>"Kynarth"),
                "from" => array("antoine.bourlart@prokonect.fr", $msg->getLastName()),
                "subject" => "My subject",
                "text" => $msg->getContent(),
                "attachment" => array(),
                "headers" => array(
                    "Content-Type"=> "text/html; charset=iso-8859-1",
                    "X-param1"=> "value1", "X-param2"=> "value2",
                    "X-Mailin-custom"=>"my custom value",
                    "X-Mailin-IP"=> "102.102.1.2", "X-Mailin-Tag" => "My tag"),
            );

            $result = $sendinblue->send_email($data);
            var_dump($result);

            exit;
            return $this->redirectToRoute('homepage');
        }

        return $this->render('AppBundle:Web/Pages:contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
