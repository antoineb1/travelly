<?php

namespace AppBundle\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\DealCommentType;
use AppBundle\Entity\DealComment;

class DealsController extends Controller
{
    /**
     * @Route("/voyage/{slug}/{id}", name="deal", requirements={"id": "\d+"})
     */
    public function viewAction(Request $request, $slug, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $deal = $em->getRepository('AppBundle:Deal')->findOneBy(['id' => $id]);

        if ($deal == null || $deal->getSlug() != $slug) {
            throw $this->createNotFoundException(
                'No deal found for id '.$id
            );
        }

        $user = $this->getUser();

        $comment = new DealComment();
        $comment->setDeal($deal);
        $comment->setUser($user);

        $form = $this->createForm(DealCommentType::class, $comment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && $user != null) {
            $em->persist($comment);
            $em->flush();
        }

        return $this->render('AppBundle:Web/Deals:view.html.twig', [
            "deal" => $deal, 'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/voyages", name="deals_search")
     */
    public function searchAction(Request $request)
    {
        $deals = [];
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        if ($request->getMethod() == "POST") {
            $finder = $this->container->get('fos_elastica.finder.app.deal');
            $keywords = $request->request->get('keywords');
            if ($keywords == '') {
                $deals = $finder->find($keywords);
                $catId = $request->request->get('category');

                if ($catId != '') {
                    foreach($deals as $key => $deal) {
                        if($deal->getCategory()->getId() != $catId) {
                            unset($deals[$key]);
                        }
                    }
                }
            }
        }

        return $this->render('AppBundle:Web/Deals:search.html.twig', [
            "deals" => $deals, "categories" => $categories
        ]);
    }


}
