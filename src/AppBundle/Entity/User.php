<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Expose
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @Expose
     * @Assert\NotBlank()
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="user")
     */
    private $deals;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="user")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity="Tag", mappedBy="user")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="DealTag", mappedBy="user")
     */
    private $dealTags;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="DealComment", mappedBy="user")
     */
    private $dealComments;

    public function __construct()
    {
        parent::__construct();
        $this->dealComments = new ArrayCollection();
        $this->deals = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->dealTags = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Add deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return User
     */
    public function addDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * Remove deal
     *
     * @param \AppBundle\Entity\Deal $deal
     */
    public function removeDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * Get deals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * Add category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return User
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return User
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add dealTag
     *
     * @param \AppBundle\Entity\DealTag $dealTag
     *
     * @return User
     */
    public function addDealTag(\AppBundle\Entity\DealTag $dealTag)
    {
        $this->dealTags[] = $dealTag;

        return $this;
    }

    /**
     * Remove dealTag
     *
     * @param \AppBundle\Entity\DealTag $dealTag
     */
    public function removeDealTag(\AppBundle\Entity\DealTag $dealTag)
    {
        $this->dealTags->removeElement($dealTag);
    }

    /**
     * Get dealTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDealTags()
    {
        return $this->dealTags;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add dealComment
     *
     * @param \AppBundle\Entity\DealComment $dealComment
     *
     * @return User
     */
    public function addDealComment(\AppBundle\Entity\DealComment $dealComment)
    {
        $this->dealComment[] = $dealComment;

        return $this;
    }

    /**
     * Remove dealComment
     *
     * @param \AppBundle\Entity\DealComment $dealComment
     */
    public function removeDealComment(\AppBundle\Entity\DealComment $dealComment)
    {
        $this->dealComment->removeElement($dealComment);
    }

    /**
     * Get dealComment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDealComment()
    {
        return $this->dealComment;
    }

    /**
     * Get dealComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDealComments()
    {
        return $this->dealComments;
    }
}
