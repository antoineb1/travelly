#!/usr/bin/env bash

for number in {1..20}
do
    php bin/console fos:user:create $number $number@example.com p@ssword$number
done
